package projetos;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
/**
 * 
 * 
 * <p>
 * Utilit�rio que manipula uma imagem
 * </p>
 * 
 * @author Lucas Nabolotnyj Martinez
 *
 */
public class Embacado extends JFrame{
	//aqui ficam os caminhos onde coloquei as imagens. o caminho relativo ao projeto � workspace\ManipulacaoImagens\img
	public static String ima_name_in  = "img\\img1.jpg";
	public static String ima_name_out  ;
	//cria os objetos que v�o recber as imagens
	private static BufferedImage ima_in;
	private static BufferedImage ima_out;

	//cria os elementos da tela principal
	private JLabel lblDesc = new JLabel();
	private JButton btnConfirma = new JButton();
	//label para alertas
	private JLabel lblConfirma = new JLabel();
	private JTextField txtCaminho = new JTextField();

	public Embacado() {
		setTitle(":: Manipulador de Imagens ::");
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//muda a aparencia da aplica�ao java
		desenhaComponentes();
		setSize(640,480);
		try {
			UIManager.setLookAndFeel(new NimbusLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private void desenhaComponentes() {
		//cria a barra de menus
		JMenuBar menu = new JMenuBar();
		//cria menu cadastro
		JMenu cadastro =  new JMenu("Efeito:");
		menu.add(cadastro);
		//cria os sub itens
		JMenuItem boxblur =  new JMenuItem("Box Blur");
		JMenuItem gaugassian =  new JMenuItem("Gaugassian Blur(3x3)");
		JMenuItem negativado =  new JMenuItem("Negativar");
		JMenuItem bordas =  new JMenuItem("Detectar Bordas");

		JMenuItem texturiza =  new JMenuItem("Texturiza");
		//vincula os subitens ao menu cadastro
		cadastro.add(boxblur);
		cadastro.addSeparator();
		cadastro.add(gaugassian);
		cadastro.addSeparator();
		cadastro.add(negativado);
		cadastro.addSeparator();
		cadastro.add(bordas);
		cadastro.addSeparator();
		cadastro.add(texturiza);
		
		//CRIANDO LAYOUT DA TELA PRINCIPAL
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		//natural height, maximum width
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 2;
		c.gridy = 1;
		add(lblDesc, c);
		c.gridx = 2;
		c.gridy = 3;
		add(txtCaminho,c);
		c.gridx = 2;
		c.gridy = 4;
		add(btnConfirma,c);
		c.gridx = 2;
		c.gridy = 5;
		add(lblConfirma,c);
		
		lblDesc.setText("Digite o caminho para a pasta."
				+ "\n ex: 'C:\\images\\imagem.jpg'");
		txtCaminho.setSize(100,10);
		btnConfirma.setText("Confirmar");
		//seta a mensagen de confirma��o como invis�vel no momento
		lblConfirma.setVisible(false);
		//nesse bot�o queremos o seguinte: quando o bot�o for clicado, valide o caminho e o coloque no ima_nome_in
		btnConfirma.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				if(txtCaminho.getText()==null){
					lblConfirma.setText("Caminho n�o especificado");
					lblConfirma.setBackground(new Color(255,0,0));
					lblConfirma.setVisible(true);
				}else if (!(new File(txtCaminho.getText())).exists()){
					lblConfirma.setText("Arquivo n�o encontrado");
					lblConfirma.setBackground(new Color(255,0,0));
					lblConfirma.setVisible(true);
				}else{
					try {
						String absolutePath = new File(txtCaminho.getText()).getAbsolutePath();
						ima_name_in = absolutePath;
						System.out.println(ima_name_in);
			    	    String filePath = absolutePath.substring(0,absolutePath.lastIndexOf(File.separator));
						ima_name_out = filePath+"\\CopiaManipulada.jpg";
						System.out.println(ima_name_out);
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}lblConfirma.setText("Caminho Salvo");
				lblConfirma.setBackground(new Color(0,100,200));
				lblConfirma.setVisible(true);

				

				//=============VVV=====Manipula��o efetiva====VVV============================================================================================================================

				//aqui � executado o processo de puxar a imagem como um objeto, e o FiltroMedia � gerado denovo porque ele �
				//ao mesmo tempo uma tela, por�m a classe inicial n�o � vis�vel e n�o tem tamanho at� o m�todo MostraImagem ser chamado
				ima_in = CarregaImagem(ima_name_in);

				}
				
			}
		});

		//vincula a��o ao menu boxblur
		boxblur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//o m�todo ProcessaImagem vai aplicar o filtro efetivamente, aue no caso se chama box blur, a imagem ficar� mais 
				//quadrada, mas � proposital(caracter�stica do filtro)
				ima_out = ProcessaImagem(ima_in);

				SalvaImagem(ima_out, ima_name_out);

				// Utilizando da Classe exibi��o para gerar um mesmo JFrame com ambas as imagens lado a lado
				Exibicao show = new Exibicao();
				show.exibirImagem(ima_in,ima_name_in, ima_out, ima_name_out);
			}
		});
		//vincula a��o ao menu gaugassian
		gaugassian.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ima_out = ProcessaImagemGaugassian(ima_in);

				SalvaImagem(ima_out, ima_name_out);

				// Utilizando da Classe exibi��o para gerar um mesmo JFrame com ambas as imagens lado a lado
				Exibicao show = new Exibicao();
				show.exibirImagem(ima_in,ima_name_in, ima_out, ima_name_out);
			}
		});
		//vincula a��o ao menu negativado
		negativado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ima_out = ProcessaImagemNegativar(ima_in);

				SalvaImagem(ima_out, ima_name_out);

				// Utilizando da Classe exibi��o para gerar um mesmo JFrame com ambas as imagens lado a lado
				Exibicao show = new Exibicao();
				show.exibirImagem(ima_in,ima_name_in, ima_out, ima_name_out);
			}
		});
		//vincula a��o ao menu bordas
				bordas.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ima_out = ProcessaImagemDetectaBordas(ima_in);

						SalvaImagem(ima_out, ima_name_out);

						// Utilizando da Classe exibi��o para gerar um mesmo JFrame com ambas as imagens lado a lado
						Exibicao show = new Exibicao();
						show.exibirImagem(ima_in,ima_name_in, ima_out, ima_name_out);
					}
				});
				//vincula a��o ao menu Terturiza
				texturiza.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						ProcessaImagemTexturiza��o(txtCaminho.getText());

					}
				});
		//adiciona um menu em tela
		//normalmente seria : add(menu);
		setJMenuBar(menu);

	}

	public static BufferedImage CarregaImagem(String image_name){
		 File file = new File(image_name);
		try {
			ima_in = ImageIO.read(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Nome da imagem: "+image_name+""
				+ "Tipo da imagem: "+ima_in.getType());
		System.out.println("Tamanho da imagem: Colunas:"+ima_in.getWidth()+"  Linhas: "+ima_in.getHeight());
		return ima_in;
	}

	public static void SalvaImagem(BufferedImage dest, String image_name){
		try{
			ImageIO.write(dest, "jpg", new File(image_name));}
		catch(Exception e){
			System.out.println("Problema gravando arquivo");
			System.exit(0);}
	}

	public static BufferedImage ProcessaImagem(BufferedImage ima_in){
		//aqui foi corrigido um bug onde o java reconheceu .jpg com o c�digo 0, e isso fica inv�lido no m�todo mais
		//� frente
		int type = ima_in.getType();  
		/*aqui eu mostro primeiramente o tipo(em c�digo; Para ver o c�digo correto � s� pesquisar nos 
		 * javadocs[https://docs.oracle.com/javase/8/docs/technotes/guides/imageio/spec/imageio_guideTOC.fm.html])
		 *  e depiois o valor rgb de cada p�xel. essa parte pode comentar para melhorar o desempenho*/ 
		System.out.println(ima_in.getType()+"\n");
		System.out.println(ima_in.getWidth()+"\n");
		System.out.println(ima_in.getHeight()+"\n");
		//onde o tipo da imagem for 0, se tornar� 5(nota��o correta de um jpg)
		if (type==0)
			type = 5;
		else
			type = type;
		//cria��o e instancia��o da imagem que receber� o resultado do filtro(img2.jpg)
		BufferedImage ima_out = new BufferedImage(ima_in.getWidth()-1, ima_in.getHeight()-1, type);
		//Raster � um tipo de estrura de dados para imagens onde os p�xels s�o representados por quadrados quando h� muito zoom
		Raster raster = ima_in.getRaster();
		WritableRaster wraster = ima_out.getRaster();
		int r,g,b;
		double valornr, valorng, valornb;
		double p[][] = new double[3][3];
		// agora, o programa vai pegar essa matriz(o nome em ingles � convalecense kernel: https://en.wikipedia.org/wiki/Kernel_(image_processing)) e inserir o numero de pixels individuais pelo numero de pixels de desfoque
		//que no caso � o tamanho da matriz(3x3)
		for (int i=0;i!=3;i++)
			for (int j=0;j!=3;j++)
				//o nome desse filtro(1/9) � box blur. 1/16 seria um gaugassian blur, que faz um blur "arredondado", que n�o 
				//fica quadrad�o quando tu pega uma imagem pequena
				p[i][j] = 1.0/9.0;

		for (int y=1;y<ima_in.getHeight()-1;y++)
			for (int x=1;x<ima_in.getWidth()-1;x++){
				// r � o valor do p�xel do meio, que � deixado de lado agora
				r = raster.getSample(x,y,0);
				//pega o valor vermelho de cada p�xel na matriz (3 por 3) com exce��o do pixel do meio,
				//e joga essa soma no valornr
				valornr = p[0][0]*(double)raster.getSample(x-1,y-1,0)+
						p[0][1]*(double)raster.getSample(x,y-1,0)+
						p[0][2]*(double)raster.getSample(x+1,y-1,0)+
						p[1][0]*(double)raster.getSample(x-1,y,0)+
						p[1][1]*(double)raster.getSample(x,y,0)+
						p[1][2]*(double)raster.getSample(x+1,y,0)+
						p[2][0]*(double)raster.getSample(x-1,y+1,0)+
						p[2][1]*(double)raster.getSample(x,y+1,0)+
						p[2][2]*(double)raster.getSample(x+1,y+1,0);
				//faz o mesmo procedimento, mas com o verde
				valorng = p[0][0]*(double)raster.getSample(x-1,y-1,1)+
						p[0][1]*(double)raster.getSample(x,y-1,1)+
						p[0][2]*(double)raster.getSample(x+1,y-1,1)+
						p[1][0]*(double)raster.getSample(x-1,y,1)+
						p[1][1]*(double)raster.getSample(x,y,1)+
						p[1][2]*(double)raster.getSample(x+1,y,1)+
						p[2][0]*(double)raster.getSample(x-1,y+1,1)+
						p[2][1]*(double)raster.getSample(x,y+1,1)+
						p[2][2]*(double)raster.getSample(x+1,y+1,1);
				//o mesmo procedimento, por�m com azul
				valornb = p[0][0]*(double)raster.getSample(x-1,y-1,2)+
						p[0][1]*(double)raster.getSample(x,y-1,2)+
						p[0][2]*(double)raster.getSample(x+1,y-1,2)+
						p[1][0]*(double)raster.getSample(x-1,y,2)+
						p[1][1]*(double)raster.getSample(x,y,2)+
						p[1][2]*(double)raster.getSample(x+1,y,2)+
						p[2][0]*(double)raster.getSample(x-1,y+1,2)+
						p[2][1]*(double)raster.getSample(x,y+1,2)+
						p[2][2]*(double)raster.getSample(x+1,y+1,2);

				//aqui o filtro pega essa soma e coloca na coordenada do p�xel central, mas em outra imagem 
				//(o 0.5 � para arredondar para cima)
				wraster.setSample(x,y,0,(int)(valornr+.5));
				System.out.println(wraster.getSample(x,y,0)+",");
				wraster.setSample(x,y,1,(int)(valorng+.5));
				System.out.println(wraster.getSample(x,y,1)+",");
				wraster.setSample(x,y,2,(int)(valornb+.5));
				System.out.println(wraster.getSample(x,y,2)+"\n\n");
			}
		System.out.println("FUNCIONOU! AHEOO!");
		return ima_out;
	}

	public static BufferedImage ProcessaImagemDetectaBordas(BufferedImage ima_in){
		//aqui foi corrigido um bug onde o java reconheceu .jpg com o c�digo 0, e isso fica inv�lido no m�todo mais
		//� frente
		int type = ima_in.getType();  
		/*aqui eu mostro primeiramente o tipo(em c�digo; Para ver o c�digo correto � s� pesquisar nos 
		 * javadocs[https://docs.oracle.com/javase/8/docs/technotes/guides/imageio/spec/imageio_guideTOC.fm.html])
		 *  e depiois o valor rgb de cada p�xel. essa parte pode comentar para melhorar o desempenho*/ 
		System.out.println(ima_in.getType()+"\n");
		System.out.println(ima_in.getWidth()+"\n");
		System.out.println(ima_in.getHeight()+"\n");
		//onde o tipo da imagem for 0, se tornar� 5(nota��o correta de um jpg)
		if (type==0)
			type = 5;
		else
			type = type;
		//cria��o e instancia��o da imagem que receber� o resultado do filtro(img2.jpg)
		BufferedImage ima_out = new BufferedImage(ima_in.getWidth()-1, ima_in.getHeight()-1, type);
		//Raster � um tipo de estrura de dados para imagens onde os p�xels s�o representados por quadrados quando h� muito zoom
		Raster raster = ima_in.getRaster();
		WritableRaster wraster = ima_out.getRaster();
		int r,g,b;
		double valornr, valorng, valornb;
		double p[][] = new double[3][3];
		// agora, o programa vai pegar essa matriz(o nome em ingles � convalecense kernel: https://en.wikipedia.org/wiki/Kernel_(image_processing)) e inserir o numero de pixels individuais pelo numero de pixels de desfoque
		//que no caso � o tamanho da matriz(3x3)
//		p[0][0]=-1;
//		p[0][1]=-1;
//		p[0][2]=-1;
//		p[1][0]=-1;
//		p[1][1]=8;
//		p[1][2]=-1;
//		p[2][0]=-1;
//		p[2][1]=-1;
//		p[2][2]=-1;
//		
		p[0][0]=-1;
		p[0][1]=2;
		p[0][2]=-1;
		p[1][0]=2;
		p[1][1]=-4;
		p[1][2]=2;
		p[2][0]=-1;
		p[2][1]=2;
		p[2][2]=-1;
		
//		p[0][0]=0;
//		p[0][1]=1;
//		p[0][2]=0;
//		p[1][0]=1;
//		p[1][1]=-4;
//		p[1][2]=1;
//		p[2][0]=0;
//		p[2][1]=1;
//		p[2][2]=0;
		for (int y=1;y<ima_in.getHeight()-1;y++)
			for (int x=1;x<ima_in.getWidth()-1;x++){
				// r � o valor do p�xel do meio, que � deixado de lado agora
				r = raster.getSample(x,y,0);
				//pega o valor vermelho de cada p�xel na matriz (3 por 3) com exce��o do pixel do meio,
				//e joga essa soma no valornr
				
				valornr = p[0][0]*(double)raster.getSample(x-1,y-1,0)+
						p[0][1]*(double)raster.getSample(x,y-1,0)+
						p[0][2]*(double)raster.getSample(x+1,y-1,0)+
						p[1][0]*(double)raster.getSample(x-1,y,0)+
						p[1][1]*(double)raster.getSample(x,y,0)+
						p[1][2]*(double)raster.getSample(x+1,y,0)+
						p[2][0]*(double)raster.getSample(x-1,y+1,0)+
						p[2][1]*(double)raster.getSample(x,y+1,0)+
						p[2][2]*(double)raster.getSample(x+1,y+1,0);
				//faz o mesmo procedimento, mas com o verde
				valorng = p[0][0]*(double)raster.getSample(x-1,y-1,1)+
						p[0][1]*(double)raster.getSample(x,y-1,1)+
						p[0][2]*(double)raster.getSample(x+1,y-1,1)+
						p[1][0]*(double)raster.getSample(x-1,y,1)+
						p[1][1]*(double)raster.getSample(x,y,1)+
						p[1][2]*(double)raster.getSample(x+1,y,1)+
						p[2][0]*(double)raster.getSample(x-1,y+1,1)+
						p[2][1]*(double)raster.getSample(x,y+1,1)+
						p[2][2]*(double)raster.getSample(x+1,y+1,1);
				//o mesmo procedimento, por�m com azul
				valornb = p[0][0]*(double)raster.getSample(x-1,y-1,2)+
						p[0][1]*(double)raster.getSample(x,y-1,2)+
						p[0][2]*(double)raster.getSample(x+1,y-1,2)+
						p[1][0]*(double)raster.getSample(x-1,y,2)+
						p[1][1]*(double)raster.getSample(x,y,2)+
						p[1][2]*(double)raster.getSample(x+1,y,2)+
						p[2][0]*(double)raster.getSample(x-1,y+1,2)+
						p[2][1]*(double)raster.getSample(x,y+1,2)+
						p[2][2]*(double)raster.getSample(x+1,y+1,2);

				//aqui o filtro pega essa soma e coloca na coordenada do p�xel central, mas em outra imagem 
				//(o 0.5 � para arredondar para cima)
				wraster.setSample(x,y,0,(int)(valornr+.5));
				System.out.println(wraster.getSample(x,y,0)+",");
				wraster.setSample(x,y,1,(int)(valorng+.5));
				System.out.println(wraster.getSample(x,y,1)+",");
				wraster.setSample(x,y,2,(int)(valornb+.5));
				System.out.println(wraster.getSample(x,y,2)+"\n\n");
			}
		System.out.println("FUNCIONA.. FUNCIONAA! MUAHAHAHAHAHAHHA!");
		return ima_out;
	}

	public static BufferedImage ProcessaImagemGaugassian(BufferedImage ima_in){
		//aqui foi corrigido um bug onde o java reconheceu .jpg com o c�digo 0, e isso fica inv�lido no m�todo mais
		//� frente
		int type = ima_in.getType();  
		/*aqui eu mostro primeiramente o tipo(em c�digo; Para ver o c�digo correto � s� pesquisar nos 
		 * javadocs[https://docs.oracle.com/javase/8/docs/technotes/guides/imageio/spec/imageio_guideTOC.fm.html])
		 *  e depiois o valor rgb de cada p�xel. essa parte pode comentar para melhorar o desempenho*/ 
		System.out.println(ima_in.getType()+"\n");
		System.out.println(ima_in.getWidth()+"\n");
		System.out.println(ima_in.getHeight()+"\n");
		//onde o tipo da imagem for 0, se tornar� 5(nota��o correta de um jpg)
		if (type==0)
			type = 5;
		else
			type = type;
		//cria��o e instancia��o da imagem que receber� o resultado do filtro(img2.jpg)
		BufferedImage ima_out = new BufferedImage(ima_in.getWidth()-1, ima_in.getHeight()-1, type);
		//Raster � um tipo de estrura de dados para imagens onde os p�xels s�o representados por quadrados quando h� muito zoom
		Raster raster = ima_in.getRaster();
		WritableRaster wraster = ima_out.getRaster();
		int r,g,b;
		double valornr, valorng, valornb;
		double p[][] = new double[3][3];
		// agora, o programa vai pegar essa matriz(o nome em ingles � convalecense kernel: https://en.wikipedia.org/wiki/Kernel_(image_processing)) e inserir o numero de pixels individuais pelo numero de pixels de desfoque
		//que no caso � o tamanho da matriz(3x3)
		p[0][0]=1;
		p[0][1]=2;
		p[0][2]=1;
		p[1][0]=2;
		p[1][1]=4;
		p[1][2]=2;
		p[2][0]=1;
		p[2][1]=2;
		p[2][2]=1;
		for (int i=0;i!=3;i++)
			for (int j=0;j!=3;j++)
				//o nome desse filtro(1/16) � Gaugassian, voce precisa de um nucleo de covalescencia 1,2,1 2,4,2 e 1,2,1 e a divis�o � por 16
				p[i][j] =(double) p[i][j]/16.0;

		for (int y=1;y<ima_in.getHeight()-1;y++)
			for (int x=1;x<ima_in.getWidth()-1;x++){
				// r � o valor do p�xel do meio, que � deixado de lado agora
				r = raster.getSample(x,y,0);
				//pega o valor vermelho de cada p�xel na matriz (3 por 3) com exce��o do pixel do meio,
				//e joga essa soma no valornr
				valornr = p[0][0]*(double)raster.getSample(x-1,y-1,0)+
						p[0][1]*(double)raster.getSample(x,y-1,0)+
						p[0][2]*(double)raster.getSample(x+1,y-1,0)+
						p[1][0]*(double)raster.getSample(x-1,y,0)+
						p[1][1]*(double)raster.getSample(x,y,0)+
						p[1][2]*(double)raster.getSample(x+1,y,0)+
						p[2][0]*(double)raster.getSample(x-1,y+1,0)+
						p[2][1]*(double)raster.getSample(x,y+1,0)+
						p[2][2]*(double)raster.getSample(x+1,y+1,0);
				//faz o mesmo procedimento, mas com o verde
				valorng = p[0][0]*(double)raster.getSample(x-1,y-1,1)+
						p[0][1]*(double)raster.getSample(x,y-1,1)+
						p[0][2]*(double)raster.getSample(x+1,y-1,1)+
						p[1][0]*(double)raster.getSample(x-1,y,1)+
						p[1][1]*(double)raster.getSample(x,y,1)+
						p[1][2]*(double)raster.getSample(x+1,y,1)+
						p[2][0]*(double)raster.getSample(x-1,y+1,1)+
						p[2][1]*(double)raster.getSample(x,y+1,1)+
						p[2][2]*(double)raster.getSample(x+1,y+1,1);
				//o mesmo procedimento, por�m com azul
				valornb = p[0][0]*(double)raster.getSample(x-1,y-1,2)+
						p[0][1]*(double)raster.getSample(x,y-1,2)+
						p[0][2]*(double)raster.getSample(x+1,y-1,2)+
						p[1][0]*(double)raster.getSample(x-1,y,2)+
						p[1][1]*(double)raster.getSample(x,y,2)+
						p[1][2]*(double)raster.getSample(x+1,y,2)+
						p[2][0]*(double)raster.getSample(x-1,y+1,2)+
						p[2][1]*(double)raster.getSample(x,y+1,2)+
						p[2][2]*(double)raster.getSample(x+1,y+1,2);

				//aqui o filtro pega essa soma e coloca na coordenada do p�xel central, mas em outra imagem 
				//(o 0.5 � para arredondar para cima)
				wraster.setSample(x,y,0,(int)(valornr+.5));
				System.out.println(wraster.getSample(x,y,0)+",");
				wraster.setSample(x,y,1,(int)(valorng+.5));
				System.out.println(wraster.getSample(x,y,1)+",");
				wraster.setSample(x,y,2,(int)(valornb+.5));
				System.out.println(wraster.getSample(x,y,2)+"\n\n");
			}
		System.out.println("FUNCIONOU");
		return ima_out;
	}

	public static BufferedImage ProcessaImagemNegativar(BufferedImage ima_in){
		//aqui foi corrigido um bug onde o java reconheceu .jpg com o c�digo 0, e isso fica inv�lido no m�todo mais
		//� frente
		int type = ima_in.getType();  
		/*aqui eu mostro primeiramente o tipo(em c�digo; Para ver o c�digo correto � s� pesquisar nos 
		 * javadocs[https://docs.oracle.com/javase/8/docs/technotes/guides/imageio/spec/imageio_guideTOC.fm.html])
		 *  e depiois o valor rgb de cada p�xel. essa parte pode comentar para melhorar o desempenho*/ 
		System.out.println(ima_in.getType()+"\n");
		System.out.println(ima_in.getWidth()+"\n");
		System.out.println(ima_in.getHeight()+"\n");
		//onde o tipo da imagem for 0, se tornar� 5(nota��o correta de um jpg)
		if (type==0)
			type = 5;
		else
			type = type;
		//cria��o e instancia��o da imagem que receber� o resultado do filtro(img2.jpg)
		BufferedImage ima_out = new BufferedImage(ima_in.getWidth()-1, ima_in.getHeight()-1, type);
		//Raster � um tipo de estrura de dados para imagens onde os p�xels s�o representados por quadrados quando h� muito zoom
		Raster raster = ima_in.getRaster();
		WritableRaster wraster = ima_out.getRaster();
		int r,g,b;
		double valornr, valorng, valornb;
		double p[][] = new double[3][3];

		for (int y=1;y<ima_in.getHeight()-1;y++)
			for (int x=1;x<ima_in.getWidth()-1;x++){
				// r � o valor do p�xel do meio, que � deixado de lado agora
				r = raster.getSample(x,y,0);
				//pega o valor vermelho de cada p�xel na matriz (3 por 3) com exce��o do pixel do meio,
				//e joga essa soma no valornr

				double varR = 255.00-(double)raster.getSample(x,y,0);
				double varG = 255.00-(double)raster.getSample(x,y,1);
				double varB = 255.00-(double)raster.getSample(x,y,2);



				//aqui o filtro pega essa soma e coloca na coordenada do p�xel central, mas em outra imagem 
				//(o 0.5 � para arredondar para cima)
				wraster.setSample(x,y,0,varR);
				System.out.println(wraster.getSample(x,y,0)+",");
				wraster.setSample(x,y,1,varG);
				System.out.println(wraster.getSample(x,y,1)+",");
				wraster.setSample(x,y,2,varB);
				System.out.println(wraster.getSample(x,y,2)+"\n\n");
			}
		System.out.println("FUNCIONOU");
		return ima_out;
	}

	public static void ProcessaImagemTexturiza��o(String img){
		
		CuboFase2 c = new CuboFase2();
		c.getTex(img);
		c.main(null);

		
		System.out.println("FUNCIONOU! AHEOO!");
	}
	
	
public static void main(String[] args) {
	new Embacado();
}


}
