package projetos;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
public class MapeamentoDeTeclasComKeyHolding3D implements KeyListener {

	//BOLEANOS PARA SETAR KEY HOLDING(FIRMAR TECLAS)
	private static boolean up, down, left, right,plus,minus;
	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
		switch(e.getExtendedKeyCode()){
		case KeyEvent.VK_UP: 
		case KeyEvent.VK_W:
			setUp(false);
			System.out.println("cima/W solto");
			break;
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_S:
			setDown(false);
			System.out.println("baixo/S solto");
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_D:
			setRight(false);
			System.out.println("direita/D solto");
			break;
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_A:
			setLeft(false);
			System.out.println("esquerda/A solto");
			break;
		case KeyEvent.VK_SPACE:
		case KeyEvent.VK_PLUS:
			setPlus(false);
			System.out.println("mais/espa�o solto");
			break;
		case KeyEvent.VK_MINUS:
		case KeyEvent.VK_SHIFT:
			setMinus(false);
			System.out.println("menos solto");
			break;
		default:
			System.out.println("tecla nao reconhecida");
			break;
		}
	}

	public void keyPressed(KeyEvent e) {
		switch(e.getExtendedKeyCode()){
		case KeyEvent.VK_UP: 
		case KeyEvent.VK_W:
			setUp(true);
			System.out.println("cima/W pressionado");
			break;
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_S:
			setDown(true);
			System.out.println("baixo/S pressionado");
			break;
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_D:
			setRight(true);
			System.out.println("direita/D pressionado");
			break;
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_A:
			setLeft(true);
			System.out.println("esquerda/A pressionado");
			break;
		case KeyEvent.VK_SPACE:
		case KeyEvent.VK_PLUS:
			setPlus(true);
			System.out.println("mais/espa�o pressionado");
			break;
		case KeyEvent.VK_MINUS:
		case KeyEvent.VK_SHIFT:
			setMinus(true);
			System.out.println("menos/SHIFT pressionado");
			break;
		}
	}

	
	public static boolean isUp() {
		return up;
	}

	public static void setUp(boolean up) {
		MapeamentoDeTeclasComKeyHolding3D.up = up;

	}

	public static boolean isDown() {
		return down;
	}

	public static void setDown(boolean down) {
		MapeamentoDeTeclasComKeyHolding3D.down = down;
	}

	public static boolean isLeft() {
		return left;
	}

	public static void setLeft(boolean left) {
		MapeamentoDeTeclasComKeyHolding3D.left = left;
	}

	public static boolean isRight() {
		return right;
	}

	public static void setRight(boolean right) {
		MapeamentoDeTeclasComKeyHolding3D.right = right;
	}

	public static boolean isPlus() {
		return plus;
	}

	public static void setPlus(boolean plus) {
		MapeamentoDeTeclasComKeyHolding3D.plus = plus;
	}

	public static boolean isMinus() {
		return minus;
	}
	public static void setMinus(boolean minus) {
		MapeamentoDeTeclasComKeyHolding3D.minus = minus;
	}




}
