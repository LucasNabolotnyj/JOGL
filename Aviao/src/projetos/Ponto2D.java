package projetos;

import javax.swing.JFrame;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

public class Ponto2D implements GLEventListener {
	private GLU glu = new GLU();
	
	public float h = 1 ;
	Ponto recebe = new Ponto();
	public void sendPa(Ponto Pa){
		recebe =  Pa;
	}

	public void display( GLAutoDrawable drawable ) {
		final GL2 gl = drawable.getGL().getGL2();


		gl.glMatrixMode( GL2.GL_PROJECTION );
		gl.glLoadIdentity();

		glu.gluPerspective( 45.0f, h, 1.0, 60.0 );
		glu.gluLookAt(0, 0, 10f, 0, 0, 0, 0, 1, 0);
		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();



		gl.glShadeModel( GL2.GL_SMOOTH );
		gl.glClearColor( 0f, 0f, 0f, 0f );
		gl.glClearDepth( 1.0f );
		gl.glEnable( GL2.GL_DEPTH_TEST );
		gl.glDepthFunc( GL2.GL_LEQUAL );
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

		// Clear The Screen And The Depth Buffer
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); 
		gl.glLoadIdentity(); // Reset The View
		
		
		gl.glColor3f(1f, 1f, 1f);
		gl.glBegin (GL2.GL_LINES);
		for (float i =-100f; i<=100f; i++){
				gl.glVertex2f(i,-100);
				gl.glVertex2f(i,100);
				gl.glVertex2f(100,i);
				gl.glVertex2f(-100,i);
		}
		gl.glEnd();
		
		

		gl.glColor3f(1f, 1f, 0f);
		gl.glBegin (GL2.GL_POINTS);

		gl.glPointSize(5f);
		gl.glVertex2f(10f,-10f);
//		gl.glVertex2f(recebe.x,recebe.z);
		gl.glEnd();
		
		
		
	}


	public void dispose( GLAutoDrawable drawable ) {
	}

	public void init( GLAutoDrawable drawable ) {

		final GL2 gl = drawable.getGL().getGL2();

		gl.glShadeModel( GL2.GL_SMOOTH );
		gl.glClearColor( 0f, 0f, 0f, 0f );
		gl.glClearDepth( 1.0f );
		gl.glEnable( GL2.GL_DEPTH_TEST );
		gl.glDepthFunc( GL2.GL_LEQUAL );
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST );
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height ) {

		// TODO Auto-generated method stub
		final GL2 gl = drawable.getGL().getGL2();
		if( height <= 0 ) 
			height = 1;

		h = ( float ) width / ( float ) height;
		gl.glViewport( 0, 0, width, height );
		gl.glMatrixMode( GL2.GL_PROJECTION );
		gl.glLoadIdentity();

		glu.gluPerspective( 45.0f, h, 1.0, 40.0 );
		glu.gluLookAt(0, 0, 10f, 0, 0, 0, 0, 1, 0);
		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();


	}

	public static void main( String[] args ) {


		// TODO Auto-generated method stub
		final GLProfile profile = GLProfile.get( GLProfile.GL2 );
		GLCapabilities capabilities = new GLCapabilities( profile );

		// The canvas
		final GLCanvas glcanvas = new GLCanvas( capabilities );
		Ponto2D triangledepthtest = new Ponto2D();

		glcanvas.addGLEventListener( triangledepthtest );
		glcanvas.setSize( 800, 800 );

		JFrame frame = new JFrame ( "CUBO" );
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(glcanvas);
		frame.setSize( frame.getContentPane().getPreferredSize() );
		frame.setVisible( true );
		final FPSAnimator animator = new FPSAnimator( glcanvas, 100, true);

		animator.start();
	}
}
