package projetos;


import javax.swing.JFrame;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

public class CuboVoador implements GLEventListener {
	private GLU glu = new GLU();
	private float rtri = 0.0f; 
	public float h = 1 ;

	private static MapeamentoDeTeclasComKeyHolding3D m =new MapeamentoDeTeclasComKeyHolding3D();

	private static float PI= 3.1415926535897932384626433832795f;

	//Specifies the radius of the circle
		static int radius = 2;

	//VARI�VEIS DE EIXO S�O CONSTANTES QUE ALTERAM DIRETAMENTE AS COORDENADAS, ATRAV�S 
	//DO M�TODO MOVIMENTAR 
	private float eixoX = 0.0f;

	private float eixoY = 0.0f;

	private float eixoZ = 0.0f;
	//******************
	//VARI�VEIS DE MOVIMENTO S�O VARI�VEIS QUE S�O ALTERADAS POR M�TODO E MUDAM
	//O VALOR DAS CONSTANTES
	private static float movX = 0.0f;
	private static float movY = 0.0f;
	private static float movZ = 0.0f;
	//*********************

	private static float camX = 0.0f;
	private static float camY = 0.0f;
	private static float camZ = -8.0f;
	//*********************
	//CRIANDO VARIAVEL DE FOR�A 
	private static float forca = 0.0f;

	public void display( GLAutoDrawable drawable ) {
		final GL2 gl = drawable.getGL().getGL2();


		gl.glMatrixMode( GL2.GL_PROJECTION );
		gl.glLoadIdentity();

		glu.gluPerspective( 45.0f, h, 1.0, 60.0 );
		//C�mera girando
		////glu.gluLookAt(Math.cos(rtri) * 6, 0, Math.sin(rtri) * 6, 0, 0, 0, 0, 1, 0);
		//primeiros 3 numeros: terceira pessoa(-10 de Z) 4 a 6 numeros: apontando
		//para o cubo(meio do cubo ou Z=0)
//				glu.gluLookAt(eixoX, (6f+eixoY), (-10f+eixoZ), eixoX, eixoY, eixoZ, 0, 1, 0);

		//CAMERA VIRANDO JUNTO COM CUBO(FICA PRETA TELA QUANDO AS VARIAVEIS ZERAM)
		//A DIRE��O QUE O CUBO VAI ANDAR VEZES 200 � A COORDENADA DA LINHA VERMELHA
		glu.gluLookAt(camX+eixoX,8f,camZ+eixoZ, eixoX, 0f, eixoZ, 0, 1, 0);

		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();

		gl.glShadeModel( GL2.GL_SMOOTH );
		gl.glClearColor( 0f, 0f, 0f, 0f );
		gl.glClearDepth( 1.0f );
		gl.glEnable( GL2.GL_DEPTH_TEST );
		gl.glDepthFunc( GL2.GL_LEQUAL );
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

		// Clear The Screen And The Depth Buffer
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); 
		gl.glLoadIdentity(); // Reset The View

		
		
//				//Set Drawing Color - Will Remain this color until otherwise specified
//				gl.glColor3f(0.2f, 0.3f, 0.5f);  //Some type of blue
//		
//				//Draw Circle
//				gl.glBegin(gl.GL_POLYGON);
//				//Change the 6 to 12 to increase the steps (number of drawn points) for a smoother circle
//				//Note that anything above 24 will have little affect on the circles appearance
//				//Play with the numbers till you find the result you are looking for
//				//Value 1.5 - Draws Triangle
//				//Value 2 - Draws Square
//				//Value 3 - Draws Hexagon
//				//Value 4 - Draws Octagon
//				//Value 5 - Draws Decagon
//				//Notice the correlation between the value and the number of sides
//				//The number of sides is always twice the value given this range
//				for(double i = 0; i < 2 * PI; i += PI / 18) //<-- Change this Value
//					gl.glVertex3f((float) (Math.cos(i) * radius)+eixoX, -1.0f, (float)Math.sin(i) * radius+eixoZ);
//				gl.glEnd();
//					//Draw Circle
	



		gl.glBegin( GL2.GL_QUADS);

		//AS COORDENADAS D�O FORMA AO CUBO(OBJETO), AS VARI�VEIS O MOVEM

		//************INICIO DESENHO CUBO*************

		//frente
		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( -1.0f+eixoX, -1.0f+eixoY, 1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Green
		gl.glVertex3f( -1.0f+eixoX, 1.0f+eixoY, 1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); // Blue
		gl.glVertex3f( 1.0f+eixoX, 1.0f+eixoY, 1.0f +eixoZ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( 1.0f+eixoX, -1.0f+eixoY, 1.0f +eixoZ);

		//tr�s
		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( -1.0f+eixoX, -1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Green
		gl.glVertex3f( -1.0f+eixoX, 1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); // Blue
		gl.glVertex3f( 1.0f+eixoX, 1.0f+eixoY, -1.0f +eixoZ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( 1.0f+eixoX, -1.0f+eixoY, -1.0f+eixoZ );

		//cima
		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Green
		gl.glVertex3f( -1.0f+eixoX, 1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); // Blue
		gl.glVertex3f( 1.0f+eixoX, 1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( 1.0f+eixoX, 1.0f+eixoY, 1.0f +eixoZ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( -1.0f+eixoX, 1.0f+eixoY, 1.0f +eixoZ); 

		//baixo
		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Green
		gl.glVertex3f( -1.0f+eixoX, -1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); // Blue
		gl.glVertex3f( 1.0f+eixoX, -1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( 1.0f+eixoX, -1.0f+eixoY, 1.0f +eixoZ); 


		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( -1.0f+eixoX, -1.0f+eixoY, 1.0f +eixoZ); 

		//esquerda
		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( -1.0f+eixoX, -1.0f+eixoY, 1.0f +eixoZ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); // Blue
		gl.glVertex3f( -1.0f+eixoX, -1.0f+eixoY, -1.0f+eixoZ );

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( -1.0f+eixoX, 1.0f+eixoY, -1.0f +eixoZ); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Green
		gl.glVertex3f( -1.0f+eixoX, 1.0f+eixoY, 1.0f +eixoZ); 

		//direita
		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( 1.0f+eixoX, -1.0f+eixoY, 1.0f +eixoZ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); // Blue
		gl.glVertex3f( 1.0f+eixoX, -1.0f+eixoY, -1.0f+eixoZ );

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); // Red
		gl.glVertex3f( 1.0f+eixoX, 1.0f+eixoY, -1.0f+eixoZ ); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Green
		gl.glVertex3f( 1.0f+eixoX, 1.0f+eixoY, 1.0f+eixoZ ); 

		gl.glEnd();

		// *****************FIM DESENHO CUBO*******************


		//********INICIO DESENHO PISO MATRIZ******************
		float i = -100.0f;
		gl.glColor3f(1f, 1f, 1f);
		gl.glBegin (GL2.GL_LINES);//static field
		while( i!=100.0f){
			gl.glVertex3f(100f,-1.0f,i);
			gl.glVertex3f(-100f,-1.0f,i);
			i=i+0.25f;
		}
		i=-100.0f;
		while( i!=100.0f){
			gl.glVertex3f(i,-1.0f,100f);
			gl.glVertex3f(i,-1.0f,-100f);
			i=i+0.25f;
		}
		gl.glVertex3f(100f,-1.0f,-100.0f);
		gl.glEnd();
		//**************FIM DESENHO PISO MATRIZ****************

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); // Red
		gl.glBegin (GL2.GL_QUADS);
		gl.glVertex3f( -10.0f, -2.0f, -10.0f); 

		gl.glVertex3f( 10.0f, -2.0f, -10.0f); 

		gl.glVertex3f( 10.0f, -2.0f, 10.0f ); 

		gl.glVertex3f( -10.0f, -2.0f, 10.0f ); 
		gl.glEnd();


		//**********CRIANDO LINHA NO MEIO DO MAPA*************
		gl.glColor3f(1f, 1f, 0f);
		gl.glBegin (GL2.GL_LINES);
		gl.glVertex3f(0f,10.0f,0.0f);
		gl.glVertex3f(0f,-1.0f,0.0f);
		//****************************************************
		//***********CRIANDO UMA LINHA NO MEIO DO CUBO MARA SERVIR DE REFERENCIA*********
		gl.glVertex3f(eixoX,10.0f,eixoZ);
		gl.glVertex3f(eixoX,0f,eixoZ);
		//****************************************************


		gl.glColor3f(1f, 0f, 0f);
		//*********CRIA��O DE LINHA QUE APONTA PARA ONDE O CUBO VAI(EM VERMELHO)********
		gl.glVertex3f(eixoX,eixoY,eixoZ);
		gl.glVertex3f(movX*200+eixoX,eixoY+forca*200,movZ*200+eixoZ);
		gl.glEnd();
		//******************************************************************
		
		
		
		
		
		//*******CHAMADA M�TODO MOVIMENTAR*************
		Movimentar(movX,movY,movZ);

		//*********INICIO DELIMITA��O MAPA***********
		if (eixoX>100)
			eixoX=100f;
		if (eixoX<-100 )
			eixoX=-100f;
		if (eixoY>5)
			eixoY=5f; 
		if (eixoY<0)
			eixoY=0f;
		if (eixoZ>100)
			eixoZ=100f;
		if (eixoZ<-100 )
			eixoZ=-100f;
		//**********FIM DELIMITA��O MAPA***************

		//**********CRIANDO UMA FOR�A DE ATRA��O***************

		if(eixoY>0)
			forca-=0.00025f;
		if(eixoY<=0)
			forca=0f;

		//**********FIM FOR�A DE ATRA��O***************

		
		//Set Drawing Color - Will Remain this color until otherwise specified
		gl.glColor3f(0.2f, 0.3f, 0.5f);  //Some type of blue

		//Draw Circle
		gl.glBegin(gl.GL_POLYGON);
		//Change the 6 to 12 to increase the steps (number of drawn points) for a smoother circle
		//Note that anything above 24 will have little affect on the circles appearance
		//Play with the numbers till you find the result you are looking for
		//Value 1.5 - Draws Triangle
		//Value 2 - Draws Square
		//Value 3 - Draws Hexagon
		//Value 4 - Draws Octagon
		//Value 5 - Draws Decagon
		//Notice the correlation between the value and the number of sides
		//The number of sides is always twice the value given this range
		for( i = 0; i < 2 * PI; i += PI / 18) //<-- Change this Value
			gl.glVertex3f((float) (Math.cos(i) * radius)+eixoX, -1.0f, ((float)Math.sin(i) * radius)+eixoZ);
		gl.glEnd();
			//Draw Circle
		
		
		//incrementos e rela��es
//		rtri += 0.001f;
		
		movX=m.RetornoX();
		movY=m.RetornoY();
		movZ=m.RetornoZ();
		eixoY+=forca;

		//**************Loop para a c�mera****************************
		if(m.RetornoX()==0 & m.RetornoZ()==0){

		}else{
			if(movZ<0f){
				camZ=movZ*200;
				if (movX==0f)
					camX =0f;
				else
					camX =(movX*200);

			}else{
				if (movX==0f)
					camX =0f;
				else
					camX =-(movX*200);
				if(movZ==0f)
					camZ =0f;
				else
					camZ =-(movZ*200);
			}
		}
		//************fim do loop para a camera***********************


	}


	//*************************************
	//M�TODO QUE GERA A ALTERA��O DAS COORDENADAS DO CUBO
	private void Movimentar(float movx, float movy, float movz) {

		eixoX+= movx;
		eixoY+= movy;
		eixoZ+= movz;
	}
	//****************************************


	@Override
	public void dispose( GLAutoDrawable drawable ) {
	}

	@Override
	public void init( GLAutoDrawable drawable ) {

		final GL2 gl = drawable.getGL().getGL2();

		gl.glShadeModel( GL2.GL_SMOOTH );
		gl.glClearColor( 0f, 0f, 0f, 0f );
		gl.glClearDepth( 1.0f );
		gl.glEnable( GL2.GL_DEPTH_TEST );
		gl.glDepthFunc( GL2.GL_LEQUAL );
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST );
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height ) {

		final GL2 gl = drawable.getGL().getGL2();
		if( height <= 0 ) 
			height = 1;

		h = ( float ) width / ( float ) height;
		gl.glViewport( 0, 0, width, height );
		gl.glMatrixMode( GL2.GL_PROJECTION );
		gl.glLoadIdentity();


		glu.gluPerspective( 45.0f, h, 1.0, 40.0 );
		glu.gluLookAt(0, 0, 10 + rtri, 0, 0, 0, 0, 1, 0);
		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();


	}

	public static void main( String[] args ) {


		// TODO Auto-generated method stub
		final GLProfile profile = GLProfile.get( GLProfile.GL2 );
		GLCapabilities capabilities = new GLCapabilities( profile );

		// The canvas
		final GLCanvas glcanvas = new GLCanvas( capabilities );
		CuboVoador testecubovoador = new CuboVoador();

		glcanvas.addGLEventListener( testecubovoador );
		glcanvas.setSize( 800, 400 );

		JFrame frame = new JFrame ( "CUBOvoador" );

		//adicionando keymapping
		frame.addKeyListener(m);

		//adicionando foco de tela(para que as teclas funcionem
		frame.setFocusableWindowState(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(glcanvas);
		frame.setSize( frame.getContentPane().getPreferredSize());

		frame.setVisible( true );

		final FPSAnimator animator = new FPSAnimator( glcanvas, 100, true);
		animator.start();
	}






}
