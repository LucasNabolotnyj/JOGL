package projetos;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLException;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;

public class CuboFase2 implements GLEventListener {




	private GLU glu = new GLU();
	private float rtri = 0.0f; 
	public float h = 1 ;
	public static float PI= 3.1415926535897932384626433832795f;
	static int radius = 2;

	//VARI�VEL DE CAMINHO DA IMAGEM USADA COMO TEXTURA
	static String ImaNameIn;


	// 					MAPEADOR DE TECLAS
	private static MapeamentoDeTeclasComKeyHolding3D m =new MapeamentoDeTeclasComKeyHolding3D();


	//				PONTO ATUAL , PONTO DE FOR�A E PONTO DE FOR�A PASSADO
	private static Ponto Pa = new Ponto();
	private static Ponto Pf = new Ponto();


	//				CRIANDO VARIAVEL DE Gravidade 
	private static float gravidade= 0.0f;

	public void display( GLAutoDrawable drawable ) {
		final GL2 gl = drawable.getGL().getGL2();


		gl.glMatrixMode( GL2.GL_PROJECTION );
		gl.glLoadIdentity();

		glu.gluPerspective( 45.0f, h, 1.0, 60.0 );
		//C�mera girando
		////glu.gluLookAt(Math.cos(rtri) * 6, 0, Math.sin(rtri) * 6, 0, 0, 0, 0, 1, 0);
		//primeiros 3 numeros: terceira pessoa(-10 de Z) 4 a 6 numeros: apontando
		//para o cubo(meio do cubo ou Z=0)
		glu.gluLookAt(Pa.x, (6f+Pa.y), (-10f+Pa.z), Pa.x, Pa.y, Pa.z, 0, 1, 0);

		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();

		gl.glShadeModel( GL2.GL_SMOOTH );
		gl.glClearColor( 0f, 0f, 0f, 0f );
		gl.glClearDepth( 1.0f );
		gl.glEnable( GL2.GL_DEPTH_TEST );
		gl.glDepthFunc( GL2.GL_LEQUAL );
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);

		// Clear The Screen And The Depth Buffer
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); 
		gl.glLoadIdentity(); // Reset The View

		//***********RECEBE A IMAGEM E RENDERIZA A MESMA PARA O USO COMO TEXTURA************
		try {
			File file = new File(ImaNameIn);
			Texture texture = TextureIO.newTexture(file, true);
			texture.enable(gl);
			texture.bind(gl);
		} catch (GLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();}
		//*********FIM RENDERIZA��O TEXTURA************

		//AGORA, O SISTEMA CRIA UMA MATRIZ COM AS COORDENADAS DO PONTO ATUAL, GERANDO O CUBO EM OUTRA REALIDADE.
		gl.glPushMatrix();
		gl.glTranslatef(Pa.x, Pa.y, Pa.z);


		//AS COORDENADAS D�O FORMA AO CUBO(OBJETO)

		//************INICIO DESENHO CUBO*************
		gl.glBegin( GL2.GL_QUADS);

		//frente

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, -1.0f, 1.0f); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, 1.0f, 1.0f); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); 
		gl.glVertex3f( 1.0f, 1.0f, 1.0f ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f );
		gl.glVertex3f( 1.0f, -1.0f, 1.0f );

		//tr�s

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, -1.0f, -1.0f ); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, 1.0f, -1.0f ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f );
		gl.glVertex3f( 1.0f, 1.0f, -1.0f ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f );
		gl.glVertex3f( 1.0f, -1.0f, -1.0f );

		//cima

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, 1.0f, -1.0f ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); 
		gl.glVertex3f( 1.0f, 1.0f, -1.0f); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( 1.0f, 1.0f, 1.0f ); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, 1.0f, 1.0f ); 

		//baixo

		gl.glColor3f( 0.0f, 1.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, -1.0f, -1.0f ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); 
		gl.glVertex3f( 1.0f, -1.0f, -1.0f); 

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( 1.0f, -1.0f, 1.0f ); 


		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, -1.0f, 1.0f ); 

		//esquerda

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, -1.0f, 1.0f ); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f );
		gl.glVertex3f( -1.0f, -1.0f, -1.0f);

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( -1.0f, 1.0f, -1.0f); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f );
		gl.glVertex3f( -1.0f, 1.0f, 1.0f); 

		//direita
		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( 1.0f, -1.0f, 1.0f); 

		gl.glColor3f( 0.0f, 0.0f, 1.0f ); 
		gl.glVertex3f( 1.0f, -1.0f, -1.0f );

		gl.glColor3f( 1.0f, 0.0f, 0.0f ); 
		gl.glVertex3f( 1.0f, 1.0f, -1.0f ); 

		gl.glColor3f( 0.0f, 1.0f, 0.0f );
		gl.glVertex3f( 1.0f, 1.0f, 1.0f); 

		gl.glEnd();

		// *****************FIM DESENHO CUBO*******************

		gl.glPopMatrix();
		//********FIM MATRIZ DO CUBO************


		//********INICIO DESENHO PISO MATRIZ******************
		float i = -100.0f;
		gl.glColor3f(1f, 1f, 1f);
		gl.glBegin (GL2.GL_LINES);
		while( i!=100.0f){
			gl.glVertex3f(100f,-1.0f,i);
			gl.glVertex3f(-100f,-1.0f,i);
			i=i+0.25f;
		}
		i=-100.0f;
		while( i!=100.0f){
			gl.glVertex3f(i,-1.0f,100f);
			gl.glVertex3f(i,-1.0f,-100f);
			i=i+0.25f;
		}
		gl.glVertex3f(100f,-1.0f,-100.0f);
		gl.glEnd();
		//**************FIM DESENHO PISO MATRIZ****************


		//DESENHANDO C�RCULO AZUL SIMULANDO SOMBRA
		gl.glColor3f(0.2f, 0.3f, 0.5f); 
		//Desenha c�rculo ao redor do cubo
		gl.glBegin(gl.GL_POLYGON);

		for( i = 0; i < 2 * PI; i += PI / 18)
			gl.glVertex3f((float) (Math.cos(i) * radius)+Pa.x, -1.0f, ((float)Math.sin(i) * radius)+Pa.z);
		gl.glEnd();
		gl.glColor3f(1f, 0f, 0f);
		//FIM DESENHO DO CIRCULO

		//QUADRADO de foto EMBAIXO
		gl.glColor3f(1f,1f,1f); 
		gl.glBegin (GL2.GL_QUADS);
		gl.glTexCoord2d(0f,0f);
		gl.glVertex3f( -10.0f, -2.0f, -10.0f); 
		gl.glTexCoord2d(1.0f,0f);
		gl.glVertex3f( 10.0f, -2.0f, -10.0f); 
		gl.glTexCoord2d(1.0f, 1f);
		gl.glVertex3f( 10.0f, -2.0f, 10.0f ); 
		gl.glTexCoord2d( 0f,1.0f);
		gl.glVertex3f( -10.0f, -2.0f, 10.0f ); 
		gl.glEnd();
		//FIM DESENHO DO QUADRADO DE FOTO EMBAIXO



		//**********CRIANDO LINHA NO MEIO DO MAPA*************
		gl.glColor3f(1f, 1f, 0f);
		gl.glBegin (GL2.GL_LINES);
		gl.glVertex3f(0f,10.0f,0.0f);
		gl.glVertex3f(0f,-1.0f,0.0f);
		//***************************************************

		//***********CRIANDO UMA LINHA NO MEIO DO CUBO MARA SERVIR DE REFERENCIA*********
		gl.glVertex3f(Pa.x,10.0f,Pa.z);
		gl.glVertex3f(Pa.x,Pa.y,Pa.z);
		gl.glEnd();
		//****************************************************




		//**********CRIANDO UMA FOR�A DE ATRA��O***************
		//LIMITE DE FOR�A DA GRAVIDADE � 0.5, OU SEJA, SE A VELOCIDADE DO OBJETO PASSAR 0,5 ELE N�O VAI CAIR
		if(Pa.y>0f)
			if(Pf.y<0.5f)
				gravidade = 0.0009f;
		//**********FIM FOR�A DE ATRA��O***************


		//				ADICIONANDO A GRAVIDADE NO DESENHO
		Pf.y-=(gravidade);


		//*******CHAMADA M�TODO MOVIMENTAR*************
		Movimentar(m.isUp(),m.isDown(),m.isLeft(), m.isRight(), m.isPlus(),m.isMinus());

		//&*******************chama m�todo Cria Angulo*************
		//		criaAng(Pf, Pfpa);
		Pa.x+=Pf.x;
		Pa.y+=Pf.y;
		Pa.z+=Pf.z;

		//*********INICIO DELIMITA��O MAPA***********
		if (Pa.x>100)
			Pa.x=100f;
		if (Pa.x<-100 )
			Pa.x=-100f;
		if (Pa.y>5)
			Pa.y=5f; 
		if (Pa.y<0)
			Pa.y=0f;
		if (Pa.z>100)
			Pa.z=100f;
		if (Pa.z<-100 )
			Pa.z=-100f;
		//**********FIM DELIMITA��O MAPA***************

	}

	//*************************************
	//M�TODO QUE GERA A ALTERA��O DAS COORDENADAS DO CUBO
	private void Movimentar(boolean Up,boolean Down,boolean Left,boolean Right, boolean Plus, boolean Minus) {
		//se acelera��o(for�a) n�o for a m�xima pode aumentar ou diminuir, sen�o decai 0.002f a cada frame
		if(Up){
			if(Pf.z<0.32f)
				Pf.z+=0.002f;
		}else if(Pf.z>0f){
			Pf.z-=0.0004f;
		}
		// -------
		if(Down){
			if(Pf.z>-0.32f)
				Pf.z-=0.002f;
		}else if(Pf.z<0f){
			Pf.z+=0.0004f;
		}
		// -------
		if(Left){
			if(Pf.x<0.32f)
				Pf.x+=0.002f;
		}else if(Pf.x>0f){
			Pf.x-=0.0004f;
		}
		// -------
		if(Right){
			if(Pf.x>-0.32f)
				Pf.x-=0.002f;
		}else if(Pf.x<0f){
			Pf.x+=0.0004f;
		}
		// -------
		if(Plus){
			if(Pf.y<0.32f)
				if(Pf.z>=0.016f||Pf.x>0.016f||Pf.z<-0.016f||Pf.x<-0.016f)
					Pf.y+=0.002f;
		}
		// -------
		if(Minus)
			if(Pf.y>-0.32f)
				Pf.y-=0.002f;

	}
	//****************************************




	// M�TODO QUE MOVE O ANGULO DE DESENHO DA MATRIZ
	public static void criaAng(Ponto a, Ponto b){
		//comprimento do ponto de for�a = raiz quadrada dos seus pontos ao quadrado
		//		Double modA = Math.sqrt((a.x*a.x)+(a.y*a.y)+(a.z*a.z));
		Double modA = Math.sqrt((a.x*a.x)+(a.y*a.y));
		//comprimento do ponto anterior 
		//		Double modB = Math.sqrt((b.x*b.x)+(b.y*b.y)+(b.z*b.z));
		Double modB = Math.sqrt((b.x*b.x)+(b.y*b.y));
		//		Double modPfpa = new Double(0d);

		//normalizando componentes = multiplicar cada coordenada pelo m�dulo
		Ponto VetorNormalizadoForca = new Ponto();
		VetorNormalizadoForca.x = (float) (a.x/modA);
		VetorNormalizadoForca.y = (float) (a.y/modA);
		//		VetorNormalizadoForca.z= (float) (a.z/modA);

		Ponto VetorNormalizadoForcaAnterior = new Ponto();
		VetorNormalizadoForcaAnterior.x = (float) (b.x/modB);
		VetorNormalizadoForcaAnterior.y = (float) (b.y/modB);
		//		VetorNormalizadoForcaAnterior.z= (float) (b.z/modB);
		//		Float PEscalar = ((a.x*b.x)+(a.y*b.y)+(a.z*b.z));

		Float PEscalar = ((a.x*b.x)+(a.y*b.y));

		//		ang = (float) Math.acos(PEscalar/(modA*modB));

	}



	public void dispose( GLAutoDrawable drawable ) {
	}

	public void init( GLAutoDrawable drawable ) {

		final GL2 gl = drawable.getGL().getGL2();

		gl.glShadeModel( GL2.GL_SMOOTH );
		gl.glClearColor( 0f, 0f, 0f, 0f );
		gl.glClearDepth( 1.0f );
		gl.glEnable( GL2.GL_DEPTH_TEST );
		gl.glDepthFunc( GL2.GL_LEQUAL );
		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST );
	}

	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height ) {

		final GL2 gl = drawable.getGL().getGL2();
		if( height <= 0 ) 
			height = 1;

		h = ( float ) width / ( float ) height;
		gl.glViewport( 0, 0, width, height );
		gl.glMatrixMode( GL2.GL_PROJECTION );
		gl.glLoadIdentity();


		glu.gluPerspective( 45.0f, h, 1.0, 40.0 );
		glu.gluLookAt(0, 0, 10 + rtri, 0, 0, 0, 0, 1, 0);
		gl.glMatrixMode( GL2.GL_MODELVIEW );
		gl.glLoadIdentity();


	}

	// CHAMADO PELO SISTEMA DE MANIPULA��O DE IMAGENS, JOGA O CAMINHO DO ARQUIVO PARA TEXTURIZA��O NESSA APLICA��O
	public static void  getTex(String imain){
		ImaNameIn = imain;
	}

	public void main( String[] args ) {


		final GLProfile profile = GLProfile.get( GLProfile.GL2 );
		GLCapabilities capabilities = new GLCapabilities( profile );

		//  canvas
		final GLCanvas glcanvas = new GLCanvas( capabilities );
		CuboFase2 testecubovoador = new CuboFase2();

		glcanvas.addGLEventListener( testecubovoador );
		glcanvas.setSize( 800, 400 );

		JFrame frame = new JFrame ( "CUBO_Fase1" );

		//adicionando keymapping
		frame.addKeyListener(m);

		//adicionando foco de tela(para que as teclas funcionem
		frame.setFocusableWindowState(true);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(glcanvas);
		frame.setSize( frame.getContentPane().getPreferredSize());

		frame.setVisible( true );

		final FPSAnimator animator = new FPSAnimator( glcanvas, 100, true);
		animator.start();
	}
}
