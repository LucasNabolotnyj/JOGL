package projetos;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
/* @autors Lucas e Renan Nabolotnyj Martinez
 * @since 29/03/2017
 * */

public class Exibicao extends JFrame {
 
    public void exibirImagem(BufferedImage image, String imagename1, BufferedImage image2, String imagename2){
        ImageIcon icon = new ImageIcon(image);
        JLabel imageLabel = new JLabel(icon);
        ImageIcon icon2 = new ImageIcon(image2);
        JLabel imageLabel2 = new JLabel(icon2);
        JFrame frame = new JFrame();
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new GridLayout(2,2));
        contentPane.add(new JScrollPane(imageLabel));
        contentPane.add(new JScrollPane(imageLabel2));
        contentPane.add(new JLabel(imagename1));
        contentPane.add(new JLabel(imagename2));
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setSize(1100, 680);
        frame.pack(); //macetinho para alinhar no centro da tela automaticamente.
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
